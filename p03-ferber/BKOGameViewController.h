//
//  BKOGameViewController.h
//  p03-ferber
//
//  Created by Itai Ferber on 2/16/16.
//  Copyright (c) 2016 iferber1. All rights reserved.
//

@import UIKit;
@import SpriteKit;

#import "BKOGameDelegate.h"

@interface BKOGameViewController : UIViewController <BKOGameDelegate>

#pragma mark - Properties
//! The label that shows the user's current score.
@property IBOutlet UILabel *scoreLabel;

//! The label that shows the number of lives the user currently has left.
@property IBOutlet UILabel *livesLabel;

//! The interactive view that hosts the game itself.
@property IBOutlet SKView *gameView;

@end
