//
//  BKOGameViewController.m
//  p03-ferber
//
//  Created by Itai Ferber on 2/16/16.
//  Copyright (c) 2016 iferber1. All rights reserved.
//

@import AudioToolbox;
#import "BKOGameViewController.h"
#import "BKOGameScene.h"

#pragma mark Private Interface
@interface BKOGameViewController ()

#pragma mark - Private Properties
//! The current game's score.
@property NSInteger score;

//! The number of lives the player currently has.
@property NSInteger lives;

@end

#pragma mark -
@implementation BKOGameViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.gameView.showsFPS = YES;
    self.gameView.showsNodeCount = YES;
    self.gameView.ignoresSiblingOrder = NO;

    self.score = 0;
    [self updateScoreLabel];
    self.lives = 3;
    [self updateLivesLabel];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    BKOGameScene *scene = [BKOGameScene sceneWithSize:self.gameView.bounds.size];
    scene.scaleMode = SKSceneScaleModeAspectFill;
    scene.gameDelegate = self;
    [self.gameView presentScene:scene];
}

#pragma mark - View Setup
- (BOOL)shouldAutorotate
{
    return NO;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

#pragma mark - Updating Labels
- (void)updateScoreLabel
{
    self.scoreLabel.text = [NSString stringWithFormat:@"Score: %lld", (long long)self.score];
}

- (void)updateLivesLabel
{
    self.livesLabel.text = [NSString stringWithFormat:@"Lives: %lld", (long long)self.lives];
}

#pragma mark - Game State
- (void)presentGameOver
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Game Over"
                                                                             message:[NSString stringWithFormat:@"You have lost. Your final score was %lld.", (long long)self.score]
                                                                                                 preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"New Game" style:UIAlertActionStyleDefault handler:nil]];

    [self presentViewController:alertController animated:YES completion:^{
        [self startNewGame];
    }];
}

- (void)presentVictory
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"You Won"
                                                                             message:[NSString stringWithFormat:@"You have won! Your final score was %lld.", (long long)self.score]
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Play Again" style:UIAlertActionStyleDefault handler:nil]];

    [self presentViewController:alertController animated:YES completion:^{
        [self startNewGame];
    }];
}

- (void)startNewGame
{
    self.score = 0;
    [self updateScoreLabel];
    self.lives = 3;
    [self updateLivesLabel];
    [(BKOGameScene *)self.gameView.scene startNewGame];
}

#pragma mark - BKOGameDelegate Actions
- (void)game:(BKOGameScene *)scene brickDidBreak:(NSInteger)value
{
    self.score += value;
    [self updateScoreLabel];
}

- (void)playerDidLoseLifeInGame:(BKOGameScene *)scene
{
    AudioServicesPlayAlertSound(kSystemSoundID_Vibrate);
    self.lives -= 1;
    [self updateLivesLabel];
    if (self.lives == 0) {
        [self presentGameOver];
    }
}

- (void)gameDidEnd:(BKOGameScene *)scene
{
    [self presentVictory];
}

@end
