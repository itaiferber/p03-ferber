//
//  BKOGameScene.h
//  p03-ferber
//
//  Created by Itai Ferber on 2/16/16.
//  Copyright (c) 2016 iferber1. All rights reserved.
//

@import SpriteKit;
#import "BKOGameDelegate.h"

@interface BKOGameScene : SKScene <SKPhysicsContactDelegate>

#pragma mark - Properties
//! A SpriteKit node representing the game paddle.
@property SKShapeNode *paddle;

//! A SpriteKit node representing the game ball.
@property SKShapeNode *ball;

//! A SpriteKit label telling the user to tap to start.
@property SKLabelNode *label;

//! A delegate to inform of game actions.
@property (weak) id<BKOGameDelegate> gameDelegate;

//! A gesture recognizer to recognize a tap to start the game.
@property UITapGestureRecognizer *tapGestureRecognizer;

//! A gesture recognizer to recognize when the user is dragging the paddle.
@property UIPanGestureRecognizer *dragGestureRecognizer;

#pragma mark - Actions
/*!
 Resets the game view to start a new round.
 */
- (void)startNewGame;

@end
