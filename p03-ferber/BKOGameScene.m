//
//  BKOGameScene.m
//  p03-ferber
//
//  Created by Itai Ferber on 2/16/16.
//  Copyright (c) 2016 iferber1. All rights reserved.
//

@import AudioToolbox;
#import "BKOGameScene.h"

#pragma mark Constants
NS_ENUM(uint32_t, BKOGameScenePhysicsCategories) {
    BKOGameScenePhysicsCategoryBall   = 1 << 0,
    BKOGameScenePhysicsCategoryPaddle = 1 << 1,
    BKOGameScenePhysicsCategoryWall   = 1 << 2,
    BKOGameScenePhysicsCategoryBrick  = 1 << 3
};

#pragma mark - Private Interface
@interface BKOGameScene ()

#pragma mark - Private Properties
@property NSMutableSet<SKShapeNode *> *bricks;

@end

#pragma mark -
@implementation BKOGameScene

#pragma mark - Initialization and Setup
static SystemSoundID tapID = 0;

+ (void)initialize
{
    if (self == [BKOGameScene class]) {
        NSURL *soundURL = [[NSBundle mainBundle] URLForResource:@"tap-fuzzy" withExtension:@"aif"];
        AudioServicesCreateSystemSoundID((__bridge CFURLRef)soundURL, &tapID);
    }
}

- (instancetype)initWithSize:(CGSize)size
{
    if (!(self = [super initWithSize:size])) {
        return nil;
    }

    // Set up paddle
    self.paddle = [SKShapeNode shapeNodeWithRect:CGRectMake(0, 0, size.width / 4, 12) cornerRadius:6];
    self.paddle.fillColor = [UIColor darkGrayColor];
    self.paddle.strokeColor = [UIColor lightGrayColor];
    self.paddle.name = @"paddle";

    // Set up paddle physics
    self.paddle.physicsBody = [SKPhysicsBody bodyWithPolygonFromPath:self.paddle.path];
    self.paddle.physicsBody.categoryBitMask = BKOGameScenePhysicsCategoryPaddle;
    self.paddle.physicsBody.collisionBitMask = BKOGameScenePhysicsCategoryBall;
    self.paddle.physicsBody.dynamic = NO;
    self.paddle.physicsBody.affectedByGravity = NO;

    // Set up ball
    self.ball = [SKShapeNode shapeNodeWithCircleOfRadius:8];
    self.ball.fillColor = [UIColor lightGrayColor];
    self.ball.name = @"ball";

    // Set up ball physics
    self.ball.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:8];
    self.ball.physicsBody.dynamic = YES;
    self.ball.physicsBody.affectedByGravity = NO;
    self.ball.physicsBody.categoryBitMask = BKOGameScenePhysicsCategoryBall;
    self.ball.physicsBody.collisionBitMask = BKOGameScenePhysicsCategoryPaddle | BKOGameScenePhysicsCategoryWall | BKOGameScenePhysicsCategoryBrick;
    self.ball.physicsBody.contactTestBitMask = BKOGameScenePhysicsCategoryPaddle | BKOGameScenePhysicsCategoryWall | BKOGameScenePhysicsCategoryBrick;
    self.ball.physicsBody.restitution = 1.0;
    self.ball.physicsBody.friction = 0.0;
    self.ball.physicsBody.linearDamping = 0.0;
    self.ball.physicsBody.angularDamping = 0.0;

    // Add gesture recognizers; don't activate either, however.
    self.tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userDidInteract:)];
    self.dragGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(userDidInteract:)];
    self.dragGestureRecognizer.maximumNumberOfTouches = 1;
    [self.dragGestureRecognizer requireGestureRecognizerToFail:self.tapGestureRecognizer];

    self.physicsWorld.contactDelegate = self;
    self.bricks = [NSMutableSet set];
    return self;
}

- (void)startNewGame
{
    for (SKShapeNode *brick in self.bricks) {
        [brick removeFromParent];
    }

    [self.bricks removeAllObjects];

    static const NSInteger numberOfColumns = 5;
    static const NSInteger numberOfRows = 5;
    CGFloat brickWidth = self.frame.size.width / numberOfColumns;
    CGFloat brickHeight = 30;
    for (NSInteger row = 0; row < numberOfRows; row += 1) {
        for (NSInteger column = 0; column < numberOfColumns; column += 1) {
            CGRect frame = CGRectMake(column * brickWidth,
                                      self.frame.size.height - (row + 2) * brickHeight,
                                      brickWidth, brickHeight);
            SKShapeNode *brick = [SKShapeNode shapeNodeWithRect:frame];
            brick.userData = [@{@"value": @((numberOfRows - row) * 10)} mutableCopy];
            brick.lineWidth = 0.0;
            switch (row) {
                case 0:
                    brick.fillColor = [UIColor redColor];
                    break;
                case 1:
                    brick.fillColor = [UIColor yellowColor];
                    break;
                case 2:
                    brick.fillColor = [UIColor greenColor];
                    break;
                case 3:
                    brick.fillColor = [UIColor cyanColor];
                    break;
                case 4:
                    brick.fillColor = [UIColor blueColor];
                    break;
                default:
                    brick.fillColor = [UIColor darkGrayColor];
                    break;
            }

            brick.physicsBody = [SKPhysicsBody bodyWithPolygonFromPath:brick.path];
            brick.physicsBody.categoryBitMask = BKOGameScenePhysicsCategoryBrick;
            brick.physicsBody.collisionBitMask = BKOGameScenePhysicsCategoryBall;
            brick.physicsBody.contactTestBitMask = BKOGameScenePhysicsCategoryBall;
            brick.physicsBody.dynamic = NO;
            brick.physicsBody.affectedByGravity = NO;
            [self.bricks addObject:brick];
            [self addChild:brick];
        }
    }

    [self continueGame];
}

- (void)continueGame
{
    // Disable any active gestures.
    self.dragGestureRecognizer.enabled = NO;
    self.tapGestureRecognizer.enabled = NO;

    // Move the ball and paddle to the current view.
    [self.ball removeFromParent];
    self.ball.position = CGPointMake(self.ball.frame.size.width,
                                     CGRectGetMidY(self.frame));
    [self addChild:self.ball];

    [self.paddle removeFromParent];
    CGFloat paddleWidth = self.frame.size.width / 4;
    self.paddle.position = CGPointMake(paddleWidth * 1.5, 30);
    [self addChild:self.paddle];

    [self.label removeFromParent];
    self.label = [SKLabelNode labelNodeWithText:@"Tap to Start"];
    self.label.fontSize = 48;
    self.label.position = CGPointMake(CGRectGetMidX(self.frame) - CGRectGetMidX(self.label.frame),
                                      CGRectGetMidY(self.frame) - CGRectGetMidY(self.label.frame));
    [self addChild:self.label];

    // Game hasn't started yet; need to tap to start.
    self.tapGestureRecognizer.enabled = YES;
    self.dragGestureRecognizer.enabled = YES;
    self.paused = YES;
}

#pragma mark - Moving Between Views
- (void)willMoveFromView:(SKView *)view
{
    [view removeGestureRecognizer:self.tapGestureRecognizer];
    [view removeGestureRecognizer:self.dragGestureRecognizer];
}

- (void)didMoveToView:(SKView *)view
{
    // Set up game physics.
    self.physicsBody = [SKPhysicsBody bodyWithEdgeLoopFromRect:self.frame];
    self.physicsBody.categoryBitMask = BKOGameScenePhysicsCategoryWall;

    [view addGestureRecognizer:self.tapGestureRecognizer];
    [view addGestureRecognizer:self.dragGestureRecognizer];
    [self startNewGame];
}

#pragma mark - Gesture Recognition
- (void)userDidInteract:(UIGestureRecognizer *)recognizer
{
    if (self.paused) {
        CGVector impulse = CGVectorMake(2.0, -2.0);
        [self.ball.physicsBody applyImpulse:impulse];
        [self.label removeFromParent];
        self.paused = NO;
    } else {
        CGPoint location = [recognizer locationInView:self.view];

        CGFloat paddleWidth = self.paddle.frame.size.width;
        CGFloat lineWidth = self.paddle.lineWidth;
        CGFloat xPosition = fmin(location.x - paddleWidth / 2,
                                 self.frame.size.width - paddleWidth - 2 * lineWidth);
        xPosition = fmax(lineWidth, xPosition);

        self.paddle.position = CGPointMake(xPosition, self.paddle.position.y);
    }
}

#pragma mark - Contact Detection
- (void)didBeginContact:(SKPhysicsContact *)contact
{
    if ((contact.bodyA == self.physicsBody || contact.bodyB == self.physicsBody) &&
        contact.contactPoint.y < self.ball.frame.size.height) {
        self.paused = YES;
        [self.gameDelegate playerDidLoseLifeInGame:self];
        [self continueGame];
    } else if ([self.bricks containsObject:(SKShapeNode *)contact.bodyA.node]) {
        SKShapeNode *brick = (SKShapeNode *)contact.bodyA.node;
        [self.gameDelegate game:self brickDidBreak:[brick.userData[@"value"] integerValue]];
        [brick removeFromParent];
        [self.bricks removeObject:brick];

        if (self.bricks.count == 0) {
            self.paused = YES;
            [self.gameDelegate gameDidEnd:self];
        }
    } else if ([self.bricks containsObject:(SKShapeNode *)contact.bodyB.node]) {
        SKShapeNode *brick = (SKShapeNode *)contact.bodyB.node;
        [self.gameDelegate game:self brickDidBreak:[brick.userData[@"value"] integerValue]];
        [brick removeFromParent];
        [self.bricks removeObject:brick];

        if (self.bricks.count == 0) {
            self.paused = YES;
            [self.gameDelegate gameDidEnd:self];
        }
    }

    AudioServicesPlaySystemSound(tapID);
}

@end
