//
//  BKOGameDelegate.h
//  p03-ferber
//
//  Created by Itai Ferber on 2/18/16.
//  Copyright © 2016 iferber1. All rights reserved.
//

@class BKOGameScene;

@protocol BKOGameDelegate <NSObject>
@required

/*!
 Called when the ball in the given game collides with a brick of the given
 value.
 */
- (void)game:(BKOGameScene *)scene brickDidBreak:(NSInteger)value;

/*!
 Called when the user fails to hit the ball and loses a life.
 */
- (void)playerDidLoseLifeInGame:(BKOGameScene *)scene;

/*!
 Called when the user breaks all available bricks.
 */
- (void)gameDidEnd:(BKOGameScene *)scene;

@end