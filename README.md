# Breakout
This is a simplistic implementation of Breakout, as specified in the CS 441 [project requirements](http://cs.binghamton.edu/~pmadden/courses/cs580/p03-breakout.html). It handles movement of the paddle, physically realistic ball movement (via SpriteKit; explicitly allowed by Prof. Madden). The game plays until all the bricks on the screen have been hit, or the user has lost three lives.

This is not meant to be an example of _good_ game design, just _quick and simple_ game design.

